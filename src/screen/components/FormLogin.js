import React from "react";
import { Form, Input, Item } from "native-base";

export const FormLogin = ({ placeholder, secureTextEntry, keyboardType }) => {
  const { itemStyle, inputStyle } = styles;
  return (
    <Form>
      <Item regular style={itemStyle}>
        <Input
          placeholder={placeholder}
          style={inputStyle}
          keyboardType={keyboardType}
          autoCapitalize={"none"}
          secureTextEntry={secureTextEntry}
        />
      </Item>
    </Form>
  );
};

const styles = {
  itemStyle: {
    borderRadius: 5,
    marginBottom: 10
  },
  inputStyle: {
    fontSize: 14,
    color: "#000"
  }
};
