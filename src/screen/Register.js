import React, { Component } from "react";
import { Text, View, TouchableOpacity, Image } from "react-native";
import {
  Container,
  FormLogin,
  ContainerSection,
  Button,
  Title,
  IconButton,
  placeholder,
  keyboardType,
  secureTextEntery
} from "./components";
import { connect } from "react-redux";

class Register extends Component {
  render() {
    const {
      headerContainer,
      headerLogo,
      blackColor,
      rowRegister,
      redColor,
      green
    } = styles;
    return (
      <Container>
        <ContainerSection style={headerContainer}>
          <Image
            source={require("../assets/headerLogo.png")}
            style={headerLogo}
          />

          <Title isheadLine iscenter style={blackColor}>
            Register
          </Title>
        </ContainerSection>

        <ContainerSection>
          <FormLogin
            placeholder={"Email"}
            keyboardType={"email-address"}
          ></FormLogin>
          <FormLogin placeholder={"Type Password"} secureTextEntry></FormLogin>
          <FormLogin
            placeholder={"Re-Type Password"}
            secureTextEntry
          ></FormLogin>
        </ContainerSection>

        <ContainerSection style={rowRegister}>
          <Title iscenter>By registering, I agree with </Title>
          <Title iscenter style={green}>
            Term and Condition
          </Title>
        </ContainerSection>
        <ContainerSection style={rowRegister}>
          <Title iscenter>and </Title>
          <Title iscenter style={green}>
            Privacy Police
          </Title>
          <Title iscenter> of Entra.</Title>
        </ContainerSection>

        <ContainerSection>
          <Button title={"Register"} />
        </ContainerSection>

        <Title iscenter>or</Title>

        <ContainerSection>
          <IconButton
            iconName={"envelope"}
            iconBgColor={"#AC3D32"}
            title={"Signup dengan Gmail"}
          />

          <IconButton
            iconName={"facebook"}
            iconBgColor={"#395185"}
            title={"Signup dengan Facebook"}
          />
        </ContainerSection>
        <ContainerSection style={rowRegister}>
          <Title iscenter style={redColor}>
            Login{" "}
          </Title>
          <Title iscenter>untuk menjelajah aplikasi</Title>
        </ContainerSection>
      </Container>
    );
  }
}

const mapStateProps = state => {
  return {};
};
export default connect(mapStateProps, {})(Register);

const styles = {
  headerContainer: {
    marginTop: 80,
    marginBottom: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    width: 200,
    resizeMode: "contain"
  },
  blackColor: {
    fontFamily: "Monterchi-Serif-Extrabold-trial",
    fontSize: 63
  },
  rowRegister: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 1
  },
  redColor: {
    color: "red"
  },
  green: {
    color: "#A9CF46"
  }
};
