import React, { Component } from "react";
import { onPress, TouchableNativeFeedback, Image } from "react-native";
import {
  Container,
  FormLogin,
  ContainerSection,
  Button,
  Title,
  IconButton
} from "./components";
import { connect } from "react-redux";

class Login extends Component {
  render() {
    const {
      headerContainer,
      headerLogo,
      blackColor,
      rowRegister,
      redColor
    } = styles;
    return (
      <Container>
        <ContainerSection style={headerContainer}>
          <Image
            source={require("../assets/headerLogo.png")}
            style={headerLogo}
          />

          <Title isheadLine iscenter style={blackColor}>
            login
          </Title>
        </ContainerSection>

        <ContainerSection>
          <FormLogin
            placeholder={"Email"}
            keyboardType={"email-address"}
          ></FormLogin>
          <FormLogin placeholder={"Password"} secureTextEntry></FormLogin>
          <Title isforget>Forget Password ?</Title>
        </ContainerSection>

        <ContainerSection>
          <Button title={"Login"} />
        </ContainerSection>

        <Title iscenter>or</Title>

        <ContainerSection>
          <IconButton
            iconName={"envelope"}
            iconBgColor={"#AC3D32"}
            title={"Login dengan Gmail"}
          />

          <IconButton
            iconName={"facebook"}
            iconBgColor={"#395185"}
            title={"Login dengan Facebook"}
          />
        </ContainerSection>
        <ContainerSection style={rowRegister}>
          <TouchableNativeFeedback
            onPress={() => this.props.navigation.navigate("Register")}
          >
            <Title iscenter style={redColor}>
              Register{" "}
            </Title>
          </TouchableNativeFeedback>
          <Title iscenter>untuk buat akun</Title>
        </ContainerSection>
      </Container>
    );
  }
}

const mapStateProps = state => {
  return {};
};
export default connect(mapStateProps, {})(Login);

const styles = {
  headerContainer: {
    marginTop: 80,
    marginBottom: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    width: 200,
    resizeMode: "contain"
  },
  blackColor: {
    fontFamily: "Monterchi-Serif-Extrabold-trial",
    fontSize: 63
  },
  rowRegister: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  redColor: {
    color: "red"
  }
};
