import React, { Component } from "react";
import { onPress, Image, TouchableNativeFeedback } from "react-native";
import {
  Container,
  ContainerSection,
  Button,
  Title,
  FormLogin
} from "./components";
import { connect } from "react-redux";

class ResetPass extends Component {
  render() {
    const {
      headerContainer,
      headerLogo,
      blackColor,
      rowRegister,
      redColor
    } = styles;
    return (
      <Container>
        <ContainerSection style={headerContainer}>
          <Image
            source={require("../assets/headerLogo.png")}
            style={headerLogo}
          />
          <Title iscenter style={blackColor}>
            Forget Password
          </Title>
          <Title iscenter>
            bla bla bla bla bla bla blablablablablabla bla bla blabla{" "}
          </Title>
          <Title>bla bla bla bla bla </Title>
        </ContainerSection>

        <ContainerSection></ContainerSection>
        <ContainerSection></ContainerSection>

        <ContainerSection>
          <FormLogin placeholder={"Email"} />
        </ContainerSection>

        <ContainerSection></ContainerSection>
        <ContainerSection></ContainerSection>

        <ContainerSection>
          <Button title={"Press Login Login Bla"} />
        </ContainerSection>

        <ContainerSection></ContainerSection>
        <ContainerSection></ContainerSection>
        <ContainerSection></ContainerSection>
        <ContainerSection></ContainerSection>

        <ContainerSection style={rowRegister}>
          <TouchableNativeFeedback
            onPress={() => this.props.navigation.navigate("Register")}
          >
            <Title iscenter style={redColor}>
              Register{" "}
            </Title>
          </TouchableNativeFeedback>
          <Title iscenter>untuk buat akun</Title>
        </ContainerSection>
      </Container>
    );
  }
}

const mapStateProps = state => {
  return {};
};
export default connect(mapStateProps, {})(ResetPass);

const styles = {
  headerContainer: {
    marginTop: 80,
    marginBottom: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    width: 200,
    resizeMode: "contain"
  },
  blackColor: {
    fontFamily: "Monterchi-Serif-Extrabold-trial",
    fontSize: 35
  },
  rowRegister: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  redColor: {
    color: "red"
  }
};
