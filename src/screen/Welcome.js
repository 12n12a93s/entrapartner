import React, { Component } from "react";
import { onPress, Image } from "react-native";
import {
  Container,
  ContainerSection,
  Button,
  Title,
  IconButton
} from "./components";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/FontAwesome";
import { Right } from "native-base";

class Welcome extends Component {
  render() {
    const { headerContainer, headerLogo, yellow, row, arrow, right } = styles;
    return (
      <Container>
        <Title style={right}>Skip</Title>
        <ContainerSection style={headerContainer}>
          <Image
            source={require("../assets/Verifikasi.png")}
            style={headerLogo}
          />
        </ContainerSection>

        <ContainerSection>
          <Title iscenter>Welcome, to Entra </Title>
          <Title iscenter>Feel Free to Join Us </Title>
        </ContainerSection>

        <ContainerSection style={row}>
          <Icon name={"long-arrow-right"} style={arrow}></Icon>
          <Title style={yellow}>Next</Title>
        </ContainerSection>
      </Container>
    );
  }
}

const mapStateProps = state => {
  return {};
};
export default connect(mapStateProps, {})(Welcome);

const styles = {
  headerContainer: {
    marginTop: 80,
    marginBottom: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    width: 200,
    resizeMode: "contain"
  },
  yellow: {
    fontFamily: "Monterchi-Serif-Extrabold-trial",
    fontSize: 18,
    color: "blue"
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  arrow: {
    width: 20
  },
  right: {
    paddingLeft: 340,
    paddingTop: 10,
    fontFamily: "Monterchi-Serif-Extrabold-trial",
    fontSize: 18
  }
};
