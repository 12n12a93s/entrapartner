import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import Drawer from "./Drawer";
import ResetPass from "../screen/ResetPass";
import DataPersonal from "../screen/DataPersonal";
import Welcome from "../screen/Welcome";
import Register from "../screen/Register";

export default createAppContainer(
  createStackNavigator(
    {
      Register,
      Drawer
    },
    {
      defaultNavigationOptions: {
        header: null
      }
    }
  )
);
